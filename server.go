package main

import (
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"math"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"

	"github.com/blendle/zapdriver"
	"github.com/julienschmidt/httprouter"
	"github.com/shawntoffel/darksky"
	"gitlab.com/tzsuite/rpc"
	"gitlab.com/tzsuite/tzutil"
	"go.uber.org/zap"
)

var seed = flag.String("seed", "", "specify a seed from which to generate the keys to sign data with")
var contract = flag.String("contract", "", "specify an address of the provider contract")

var rpcURL = flag.String("rpcurl", "", "specify the base url of the tezos-node RPC")

// var verbose = flag.Bool("v", false, "print out more information")
var debug = flag.Bool("debug", false, "print out debug information")
var log *zap.SugaredLogger

var apiKey = flag.String("apikey", "", "specify a key for Dark Sky API")

var client darksky.DarkSky

var rpcClient *rpc.RPC
var keys tzutil.Keys

// profit margin
const margin = 0.05

const contentType = "Content-Type"
const appJSON = "application/json; charset=UTF-8"

type policyQuote struct {
	PayReq       json.RawMessage `json:"payreq"`
	TrezorParams string          `json:"trezorParams"`
	Arg          string          `json:"arg"`
	policyParams
}

type policyParams struct {
	Payout     int64   `json:"payout"`
	Price      int64   `json:"price"`
	Expiration int64   `json:"expiration"`
	T          int64   `json:"t"`
	Latitude   float64 `json:"lat"`
	Longitude  float64 `json:"lon"`
	Nonce      string  `json:"nonce"`
	req        darksky.ForecastRequest
}

var info struct {
	Provider string `json:"provider"`
	Proxy    string `json:"proxy"`
	Oracle   string `json:"oracle"`
}

func main() {
	flag.Parse()
	cloudConf()
	client = darksky.New(*apiKey)

	loggerFunc := zapdriver.NewProduction
	if *debug {
		loggerFunc = zapdriver.NewDevelopment
	}
	logger, err := loggerFunc()
	if err != nil {
		fmt.Fprintf(os.Stderr, "setting up zap: %v\n", err)
		os.Exit(1)
	}
	defer logger.Sync() // flushes buffer, if any
	log = logger.Sugar()

	// regenerate keys from seed
	keys, err = tzutil.KeysFromStringSeed(*seed)
	if err != nil {
		log.Fatalf("keys: %v", err)
	}

	log.Debugf("keys: %#v\n", keys)

	e := make(chan error)
	shutdown := make(chan struct{})
	go func(e chan error, shutdown chan struct{}) {
		err := <-e
		if err != nil {
			fmt.Println("Received an error...")
			fmt.Println("Shutting down...")
			close(shutdown)
			log.Info("Waiting a bit...")
			time.Sleep(time.Millisecond * 500)
			log.Fatalf("goroutine error from rpc: %v", err)
		}
	}(e, shutdown)

	rpcClient, err = rpc.New(ConfAlphanet(*rpcURL, log), nil, shutdown, e)
	if err != nil {
		log.Fatalf("creating client: %v", err)
	}

	go shutdowner(shutdown)

	// check that the provider contract exists
	err = rpcClient.CheckContract(*contract)
	if err != nil {
		log.Fatalf("checking that provider contract exists: %v", err)
	}
	info.Provider = *contract

	provider, err := rpcClient.Contract(*contract)
	if err != nil {
		log.Fatalf("getting details of provider contract: %v", err)
	}
	// make sure we have to proper keys
	pubkey, err := getProviderPubkey(provider.Script.Storage)
	if err != nil {
		log.Fatalf("extracting provider pubkey: %v", err)
	}
	if keys.PublicStr != pubkey {
		log.Fatalf("keys from seed don't match contract keys: seed (have) %v contract (want) %v", keys.PublicStr, pubkey)
	}
	// parse provider storage for addresses of proxy and oracle contracts
	proxy, err := getProxyAddress(provider.Script.Storage)
	if err != nil {
		log.Fatalf("extracting proxy address: %v", err)
	}
	info.Proxy = proxy
	oracle, err := getOracleAddress(provider.Script.Storage)
	if err != nil {
		log.Fatalf("extracting oracle address: %v", err)
	}
	info.Oracle = oracle

	router := httprouter.New()
	router.GET("/quote", optionsHeaders(getQuote))
	router.GET("/info", optionsHeaders(getInfo))

	fmt.Println("Listening for policy requests.")
	log.Fatal(http.ListenAndServe(":8080", router))
}

func optionsHeaders(protected httprouter.Handle) httprouter.Handle {
	return httprouter.Handle(func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "OPTIONS, GET")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		protected(w, r, ps)
	})
}

func getInfo(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	log.Debugf("%v: %s request received\n", time.Now().UTC().Format(time.RFC3339), "info")
	resp, err := json.Marshal(&info)
	if err != nil {
		fmt.Printf("%s: error: marshaling response: %v\n", time.Now().Format(time.RFC3339), err)
		http.Error(w, "500 internal server error", http.StatusInternalServerError)
		return
	}
	w.Header().Set(contentType, appJSON)
	w.Write(resp)
}

func getQuote(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	log.Debugf("%v: %s request received\n", time.Now().UTC().Format(time.RFC3339), "quote")
	var p policyParams
	p, err := parseRequest(r.URL.Query())
	if err != nil {
		http.Error(w, fmt.Sprintf("400 %v", err), http.StatusBadRequest)
		return
	}
	prob, err := getProbability(client, p.req)
	if err != nil {
		http.Error(w, fmt.Sprintf("400 %v", err), http.StatusBadRequest)
		return
	}

	// always round up so that we don't accidentaly lose money
	p.Price = int64(math.Ceil((1 + margin) * float64(p.Payout) * prob))

	// always charge at least 0.1 tez
	if p.Price < 100000 {
		p.Price = 100000
	}

	// add 1 tez that will be needed for the oracle fee
	p.Price += 1000000

	// quote is valid for 15 minutes
	p.Expiration = time.Now().Add(time.Minute * 15).Unix()

	// generate a random identifier for the policy
	b := make([]byte, 20)
	_, err = rand.Read(b)
	if err != nil {
		http.Error(w, fmt.Sprintf("500 %v", err), http.StatusInternalServerError)
		return
	}
	p.Nonce = hex.EncodeToString(b)

	hdi := prepareData(p)
	_, sig, err := rpcClient.HashAndSignData(hdi, keys)
	if err != nil {
		fmt.Printf("%s: error: signing data: %v\n", time.Now().Format(time.RFC3339), err)
		http.Error(w, "500 internal server error", http.StatusInternalServerError)
		return
	}

	var q policyQuote
	q.policyParams = p
	q.PayReq = json.RawMessage(fmt.Sprintf(`{
		"kind": "transaction",
		"amount": "%d",
		"destination": "%s",
		"parameters": {
			"prim": "Right",
			"args": [
				{
					"prim": "Pair",
					"args": [
						{
							"string": "%s"
						},
						%s
					]
				}
			]
		}
	}`, q.Price, *contract, sig, hdi.Data))

	q.Arg = fmt.Sprintf(`(Right (Pair "%s" %s))`, sig, prepSExpr(p))

	trezorParam, _, err := rpcClient.PackData(prepareTrezorParam(p, sig))
	if err != nil {
		fmt.Printf("%s: error: packing data: %v\n", time.Now().Format(time.RFC3339), err)
		http.Error(w, "500 internal server error", http.StatusInternalServerError)
		return
	}
	q.TrezorParams = hex.EncodeToString(trezorParam)

	resp, err := json.Marshal(&q)
	if err != nil {
		fmt.Printf("%s: error: marshaling response: %v\n", time.Now().Format(time.RFC3339), err)
		http.Error(w, "500 internal server error", http.StatusInternalServerError)
		return
	}
	w.Header().Set(contentType, appJSON)
	w.Write(resp)
}

func prepSExpr(p policyParams) string {
	tsExp := time.Unix(p.Expiration, 0).UTC().Format(time.RFC3339)
	tsT := time.Unix(p.T, 0).UTC().Format(time.RFC3339)
	lat := fmt.Sprintf("%d", int(round(p.Latitude*10000)))
	lon := fmt.Sprintf("%d", int(round(p.Longitude*10000)))
	return fmt.Sprintf(`(Pair "%s" (Pair 0x%s (Pair (Pair %d %d) (Pair (Pair %s %s) "%s"))))`, tsExp, p.Nonce, p.Payout, p.Price, lat, lon, tsT)
}

func parseRequest(values url.Values) (policyParams, error) {
	var p policyParams
	for param, value := range values {
		if len(value) > 1 {
			// expected a single value
			return p, fmt.Errorf("invalid query params")
		}
		switch param {
		case "lat", "lon":
			l, err := strconv.ParseFloat(value[0], 64)
			if err != nil {
				return p, fmt.Errorf("converting location: %v", err)
			}
			l = roundPlaces(l, 4)
			if param == "lat" {
				p.Latitude = l
				p.req.Latitude = darksky.Measurement(l)
			} else if param == "lon" {
				p.Longitude = l
				p.req.Longitude = darksky.Measurement(l)
			}
		case "time":
			ts, err := strconv.ParseInt(value[0], 10, 64)
			if err != nil {
				return p, fmt.Errorf("converting time: %v", err)
			}
			t := time.Unix(ts, 0)
			cutoff := time.Now().Add(time.Minute * 3)
			if cutoff.After(t) {
				// can't insure less then 3 minutes before requested policy time
				return p, fmt.Errorf("requested time too soon")
			}
			nextWeek := time.Now().AddDate(0, 0, 7)
			if nextWeek.Before(t) {
				// darksky doesn't provide forecasts further out than a week, so we can't calculate the price
				return p, fmt.Errorf("requested time too far in the future")
			}
			p.T = ts
			p.req.Time = darksky.Timestamp(ts)
		case "payout":
			payout, err := strconv.ParseInt(value[0], 10, 64)
			if err != nil {
				return p, fmt.Errorf("converting payout: %v", err)
			}
			p.Payout = payout
		}
	}
	return p, nil
}

func getProbability(client darksky.DarkSky, request darksky.ForecastRequest) (float64, error) {
	// use metric units
	request.Options.Units = "si"
	resp, err := client.Forecast(request)
	if err != nil {
		return 0, err
	}
	hourly := resp.Hourly.Data
	var d darksky.DataPoint
	for _, h := range hourly {
		if request.Time < h.Time {
			break
		}
		d = h
	}
	prob := float64(d.PrecipProbability)
	log.Debug("probability from darksky API:", prob)
	return prob, nil
}
