package main

import (
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/tzsuite/rpc"
)

// ./tezos-client -l -P 80 -A zeronet-node.tzscan.io hash data '(Pair "2018-11-04T14:55:03Z" (Pair (Pair 123510000 100000) (Pair (Pair 123450 567890) "2018-11-04T14:47:23Z")))' of type '(pair timestamp (pair (pair mutez mutez) (pair (pair int int) timestamp)))'

func prepareData(p policyParams) rpc.HashDataInput {
	tsExp := time.Unix(p.Expiration, 0).UTC().Format(time.RFC3339)
	tsT := time.Unix(p.T, 0).UTC().Format(time.RFC3339)
	lat := fmt.Sprintf("%d", int(round(p.Latitude*10000)))
	lon := fmt.Sprintf("%d", int(round(p.Longitude*10000)))
	data := fmt.Sprintf(`{ "prim": "Pair",
	"args":
		[ { "string": "%s" },
			{ "prim": "Pair",
				"args":
					[ { "bytes": "%s" },
						{ "prim": "Pair",
							"args":
								[ { "prim": "Pair",
										"args":
											[ { "int": "%d" }, { "int": "%d" } ] },
									{ "prim": "Pair",
										"args":
											[ { "prim": "Pair",
													"args":
														[ { "int": "%s" },
															{ "int": "%s" } ] },
												{ "string": "%s" } ] } ] } ] } ] }`,
		tsExp, p.Nonce, p.Payout, p.Price, lat, lon, tsT)
	dataType := `{ "prim": "pair",
	"args":
		[ { "prim": "timestamp" },
			{ "prim": "pair",
				"args":
					[ { "prim": "bytes" },
						{ "prim": "pair",
							"args":
								[ { "prim": "pair",
										"args":
											[ { "prim": "mutez" }, { "prim": "mutez" } ] },
									{ "prim": "pair",
										"args":
											[ { "prim": "pair",
													"args":
														[ { "prim": "int" }, { "prim": "int" } ] },
												{ "prim": "timestamp" } ] } ] } ] } ] }`
	return rpc.HashDataInput{
		Data: json.RawMessage(data),
		Type: json.RawMessage(dataType),
	}
}

func prepareTrezorParam(p policyParams, sig string) rpc.HashDataInput {
	tsExp := time.Unix(p.Expiration, 0).UTC().Format(time.RFC3339)
	tsT := time.Unix(p.T, 0).UTC().Format(time.RFC3339)
	lat := fmt.Sprintf("%d", int(round(p.Latitude*10000)))
	lon := fmt.Sprintf("%d", int(round(p.Longitude*10000)))
	data := fmt.Sprintf(`
	{ "prim": "Right",
	"args":
		[ { "prim": "Pair",
			"args":
			[ { "string":
					"%s" },
				{ "prim": "Pair",
				"args":
					[ { "string": "%s" },
					{ "prim": "Pair",
						"args":
						[ { "bytes":
								"%s" },
							{ "prim": "Pair",
							"args":
								[ { "prim": "Pair",
									"args":
									[ { "int": "%d" },
										{ "int": "%d" } ] },
								{ "prim": "Pair",
									"args":
									[ { "prim": "Pair",
										"args":
											[ { "int": "%s" },
											{ "int": "%s" } ] },
										{ "string": "%s" } ] } ] } ] } ] } ] } ] }`,
		sig, tsExp, p.Nonce, p.Payout, p.Price, lat, lon, tsT)
	dataType := `{ "prim": "or",
	"args":
	  [ { "prim": "or",
		  "args":
			[ { "prim": "pair",
				"args":
				  [ { "prim": "bytes" },
					{ "prim": "pair",
					  "args": [ { "prim": "bytes" }, { "prim": "nat" } ] } ] },
			  { "prim": "or",
				"args":
				  [ { "prim": "bytes" },
					{ "prim": "or",
					  "args":
						[ { "prim": "pair",
							"args":
							  [ { "prim": "signature" },
								{ "prim": "pair",
								  "args":
									[ { "prim": "mutez" },
									  { "prim": "key" } ] } ] },
						  { "prim": "pair",
							"args":
							  [ { "prim": "signature" },
								{ "prim": "contract",
								  "args":
									[ { "prim": "pair",
										"args":
										  [ { "prim": "bytes" },
											{ "prim": "pair",
											  "args":
												[ { "prim": "bytes" },
												  { "prim": "nat" } ] } ] } ] } ] } ] } ] } ] },
		{ "prim": "pair",
		  "args":
			[ { "prim": "signature" },
			  { "prim": "pair",
				"args":
				  [ { "prim": "timestamp" },
					{ "prim": "pair",
					  "args":
						[ { "prim": "bytes" },
						  { "prim": "pair",
							"args":
							  [ { "prim": "pair",
								  "args":
									[ { "prim": "mutez" },
									  { "prim": "mutez" } ] },
								{ "prim": "pair",
								  "args":
									[ { "prim": "pair",
										"args":
										  [ { "prim": "int" },
											{ "prim": "int" } ] },
									  { "prim": "timestamp" } ] } ] } ] } ] } ] } ] }`
	return rpc.HashDataInput{
		Data: json.RawMessage(data),
		Type: json.RawMessage(dataType),
	}
}
