package main

import (
	"encoding/json"
	"fmt"
	"math"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/savaki/jq"
)

func round(f float64) float64 {
	if f < 0 {
		return math.Ceil(f - 0.5)
	}
	return math.Floor(f + 0.5)
}

func roundPlaces(f float64, places int) float64 {
	shift := math.Pow(10, float64(places))
	return round(f*shift) / shift
}

// reverse returns its argument string reversed rune-wise left to right.
func reverse(s string) string {
	r := []rune(s)
	for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}
	return string(r)
}

// catch shutdown signals and shutdown gracefully
func shutdowner(shutdown chan struct{}) {
	signals := make(chan os.Signal)
	signal.Notify(signals, os.Interrupt, syscall.SIGTERM)
	s := <-signals
	fmt.Printf("Received an interrupt: %v\n", s)
	fmt.Println("Shutting down...")
	close(shutdown)
	log.Info("Waiting a bit...")
	time.Sleep(time.Millisecond * 500)
	log.Info("Exiting...")
	os.Exit(0)
}

var parseProviderPubkey jq.Op
var parseOracleContract jq.Op
var parseProxyContract jq.Op

func init() {
	parseProviderPubkey = jq.Must(jq.Parse(".args.[0].args.[0].args.[0].args.[0].string"))
	parseOracleContract = jq.Must(jq.Parse(".args.[0].args.[1].args.[0].string"))
	parseProxyContract = jq.Must(jq.Parse(".args.[0].args.[1].args.[1].string"))
}

func getProviderPubkey(storage json.RawMessage) (string, error) {
	v, err := parseProviderPubkey.Apply(storage)
	if err != nil {
		return "", fmt.Errorf("parsing storage: %v", err)
	}
	pubkey := strings.TrimFunc(string(v), func(r rune) bool { return r == '"' })
	log.Info("Provider public key:", pubkey)
	return pubkey, nil
}

func getOracleAddress(storage json.RawMessage) (string, error) {
	v, err := parseOracleContract.Apply(storage)
	if err != nil {
		return "", fmt.Errorf("parsing storage: %v", err)
	}
	oracleContract := strings.TrimFunc(string(v), func(r rune) bool { return r == '"' })
	log.Info("Oracle contract:", oracleContract)
	return oracleContract, nil
}

func getProxyAddress(storage json.RawMessage) (string, error) {
	v, err := parseProxyContract.Apply(storage)
	if err != nil {
		return "", fmt.Errorf("parsing storage: %v", err)
	}
	proxyContract := strings.TrimFunc(string(v), func(r rune) bool { return r == '"' })
	log.Info("Proxy contract:", proxyContract)
	return proxyContract, nil
}
