package main

import (
	"fmt"
	"runtime"

	"github.com/blendle/zapdriver"
	"gitlab.com/tzsuite/rpc"
	"go.uber.org/zap"
)

func trace3() string {
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	_, _ = frames.Next()
	frame, _ := frames.Next()
	// return fmt.Sprintf("%s,:%d %s", frame.File, frame.Line, frame.Function)
	return frame.Function
}

func frame3() runtime.Frame {
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	_, _ = frames.Next()
	frame, _ := frames.Next()
	return frame
}

type logger struct {
	*zap.SugaredLogger
}

func (l logger) Debugf(pattern string, args ...interface{}) {
	f := frame3()
	loc := zapdriver.SourceLocation(f.PC, f.File, f.Line, true)
	l.SugaredLogger.With(loc).Debugf(pattern, args...)
}

func (l logger) Infof(pattern string, args ...interface{}) {
	f := frame3()
	loc := zapdriver.SourceLocation(f.PC, f.File, f.Line, true)
	l.SugaredLogger.With(loc).Infof(pattern, args...)
}

func (l logger) Debugln(args ...interface{}) {
	f := frame3()
	loc := zapdriver.SourceLocation(f.PC, f.File, f.Line, true)
	l.SugaredLogger.Debugw(fmt.Sprint(args...), loc)
}

func (l logger) Infoln(args ...interface{}) {
	f := frame3()
	loc := zapdriver.SourceLocation(f.PC, f.File, f.Line, true)
	l.SugaredLogger.Infow(fmt.Sprint(args...), loc)
}

// ConfAlphanet is a preset config for Tezos alphanet.
func ConfAlphanet(baseURL string, l *zap.SugaredLogger) rpc.Config {
	c := rpc.ConfAlphanet(baseURL)
	c.Logger = logger{
		SugaredLogger: l,
	}
	return c
}
