package main

import (
	"os"

	"google.golang.org/appengine"
)

func cloudConf() {
	if appengine.IsAppEngine() {
		if env := os.Getenv("SEED"); env != "" {
			*seed = env
		}
		if env := os.Getenv("CONTRACT"); env != "" {
			*contract = env
		}
		if env := os.Getenv("RPCURL"); env != "" {
			*rpcURL = env
		}
		if env := os.Getenv("APIKEY"); env != "" {
			*apiKey = env
		}
		// if env := os.Getenv("VERBOSE"); env != "" {
		// 	*verbose = true
		// }
		if env := os.Getenv("DEBUG"); env != "" {
			*debug = true
		}
	}
}
